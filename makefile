SHELL := /bin/bash
CODE_DIR=build/code
BUILD_DIR=build/code/runtime
DOC_DIR=build/docs
SRC_DIR=src/runtime
PWD=$(shell pwd)
LINT_FILE=${PWD}/${CODE_DIR}/lint_output
EXIT_FILE=${PWD}/exit.txt
STATUS=0

all:  build #rmv-fst-ln

rmv-fst-ln: build
	(tail -n +2 ${PWD}/${CODE_DIR}/runtime/bin/www > ${PWD}/${CODE_DIR}/runtime/bin/tempfile; mv ${PWD}/${CODE_DIR}/runtime/bin/tempfile ${PWD}/${CODE_DIR}/runtime/bin/www)

init:
	./init.sh

build: init
	make -f pub-make -k build

build-with-basic-infra: init clone-basic-infra build
	echo "build with basic infra"

build-with-full-infra: init clone-exp-infra build
	echo "build with full infra"

clone-basic-infra: wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-exp-infra: clone-basic-infra clone-exp-repos
	echo "exp infrastructure put in place"

clone-basic-themes:
	make -f pub-make -k clone-basic-themes

clone-exp-repos:
	make -f pub-make -k clone-exp-repos

clone-exporters:
	make -f pub-make -k clone-exporters

wget-orgs:
	make -f pub-make -k wget-orgs

clean:
	make -f pub-make clean
	(rm -rf build; rm -rf ./exp-publisher)

clean-infra:
	make -f pub-make clean-infra

