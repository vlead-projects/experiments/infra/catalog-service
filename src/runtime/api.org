#+TITLE: Design for the Catalog Service
#+AUTHOR: VLEAD
#+DATE: [2018-06-12 Wed]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* APIs
This guide explains how to use the Catalog Service APIs to
get the Catalogs for an experiment.

** POST Request
The following API helps to post to the Catalog Service

*** Type of Request: POST
*** URL: =/catalog=
*** Parameter Format: JSON
*** Parameter Example:
#+BEGIN_EXAMPLE
{
	"document_catalog":{
			"rcatalog": {
				"id": "",
				"reference_id": "",
				"resources": ["merge-sort-img", "merge-sort-img-2"],
				"refinment_map": []
			},
			"merge-sort-img": {
				"id": "merge-sort-img",
				"type": "image",
				"url": "https://www.w3schools.in/wp-content/uploads/2016/09/Merge-Sort-Technique-1.png"
			},
			"merge-sort-img-2":{
				"id": "merge-sort-img-2",
				"type": "image",
				"url": "http://rosalind.info/media/problems/ms/mergesortexample.png"
			}
	},
	"realization_catalog":{
			"rcatalog": {
				"id": "",
				"reference_id": "",
				"resources": ["merge-sort-img", "merge-sort-img-2"],
				"refinment_map": []
			},
			"merge-sort-img": {
				"id": "merge-sort-img",
				"type": "image",
				"url": "https://www.w3schools.in/wp-content/uploads/2016/09/Merge-Sort-Technique-1.png"
			},
			"merge-sort-img-2":{
				"id": "merge-sort-img-2",
				"type": "image",
				"url": "http://rosalind.info/media/problems/ms/mergesortexample.png"
			}
	},
	"typeOfDocument":"A1",
	"expId":"merge-sort"
}
#+END_EXAMPLE
*** Success Response
- Code: 200
- Body: Documents uploaded

*** Error Response
- Code: 500
- Body: None of the fields can be undefined

** GET Request
The following API helps to get all the catalogs of a
particular experiment

*** Type of Request: GET
*** URL: =/catalog/:experiment_id=
*** Returned Body(Example):
#+BEGIN_EXAMPLE
{
    "exp_id": "merge-sort",
    "catalogs": {
        "merge-sort-A1": {
            "document_catalog": "{\"rcatalog\":{\"id\":\"\",\"reference_id\":\"\",\"resources\":[\"merge-sort-img\",\"merge-sort-img-2\"],\"refinment_map\":[]},\"merge-sort-img\":{\"id\":\"merge-sort-img\",\"type\":\"image\",\"url\":\"https://www.w3schools.in/wp-content/uploads/2016/09/Merge-Sort-Technique-1.png\"},\"merge-sort-img-2\":{\"id\":\"merge-sort-img-2\",\"type\":\"image\",\"url\":\"http://rosalind.info/media/problems/ms/mergesortexample.png\"}}",
            "realization_catalog": "{\"rcatalog\":{\"id\":\"\",\"reference_id\":\"\",\"resources\":[\"merge-sort-img\",\"merge-sort-img-2\"],\"refinment_map\":[]},\"merge-sort-img\":{\"id\":\"merge-sort-img\",\"type\":\"image\",\"url\":\"https://www.w3schools.in/wp-content/uploads/2016/09/Merge-Sort-Technique-1.png\"},\"merge-sort-img-2\":{\"id\":\"merge-sort-img-2\",\"type\":\"image\",\"url\":\"http://rosalind.info/media/problems/ms/mergesortexample.png\"}}"
        }
    }
}
#+END_EXAMPLE
